import requests
import datetime as dt
from tqdm import tqdm_notebook

# helper functions

def _chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

# YT class

class YT(object):
    """
    Tool for extract data from YouTube API.

    Parameters
    ----------
    self.api_key : str
        YouTube API key to consume data.
    """

    def __init__(self, api_key):
        self.path = "https://www.googleapis.com/youtube/v3/"
        self.api_key = api_key

    def get_channels(self, channel_ids):
        """
        Retrieve Youtube channel infos.

        Parameters
        ----------
        channel_ids: list
            A list of channel IDs.

        Returns
        -------
        list
            A list of items with channel infos, including some statistics.        
        """
        url =  self.path + 'channels'
        payload = {'part': 'id,snippet,brandingSettings,contentDetails,invideoPromotion,statistics,topicDetails',
                   'key': self.api_key,
                   'maxResults': 50,
                   'id': ''}
        result = []
        for chunk in _chunks(channel_ids, 50):
            payload['id'] = ','.join(chunk)
            response = requests.get(url, params=payload).json()['items']
            result.extend(response) 
        return result    
    
    def get_channel_ids(self, usernames):
        """
        Retrieve Youtube channel IDs from usernames.

        Parameters
        ----------
        usernames: list
            A list of channels usernames (www.youtube.com/user/{username}).

        Returns
        -------
        list
            A list of channel IDs. 
        """
        url =  self.path + 'channels'
        payload = {'part': 'id',
                   'key': self.api_key,
                   'forUsername': ''}
        result = []
        for username in usernames:
            payload['forUsername'] = username
            response = requests.get(url, params=payload).json()['items'][0]['id']
            result.append(response)
        return result  

    def get_channel_ids_from_url(self, urls):
        """
        Retrieve channel IDs from a list of channel URLs. They can bem either user or channel pages.
        """
        url_types={'user': [], 'channel': []}
        for url in urls:
            type_and_name = url.split('/')[-2:]
            url_types[type_and_name[0]].append(type_and_name[1])
        return url_types['channel'] + self.get_channel_ids(url_types['user'])
    
    def get_video_ids(self, channel_id):
        """
        Retrieve video ids from a Youtube channel.

        Parameters
        ----------
        channel_id: str
            Youtube channel id.

        Returns
        -------
        list
            A list of video ids. 
        """
        url = self.path + 'search'
        payload = {'part': 'id,snippet',
                   'type': 'video',
                   'key': self.api_key,
                   'channelId': channel_id,
                   'maxResults': 50,
                   'order': 'date'}
        result = []
        has_more_pages = True
        while has_more_pages:
            response = requests.get(url, params=payload).json()
            if response['items']:
                for i in response['items']:
                    result.append(i['id']['videoId'])
                old_date = response['items'][-1]['snippet']['publishedAt']
                old_date = dt.datetime.strptime(old_date, '%Y-%m-%dT%H:%M:%S.%fZ')
                old_date = (old_date - dt.timedelta(seconds=1)).isoformat() + "Z"
                payload['publishedBefore'] = old_date     
            else:
                has_more_pages = False
        return result

    def get_videos(self, video_ids):
        """
        Retrieve Youtube video infos from API.

        Parameters
        ----------
        video_ids: list
            A list of Youtube video ids.

        Returns
        -------
        list
            A list of video objects. 
        """
        url = self.path+'videos'
        payload = {'part': 'id,snippet,contentDetails,player,recordingDetails,statistics,status,topicDetails',
                   'id': '',
                   'key': self.api_key}
        result = []
        for chunk in _chunks(video_ids, 50):
            payload['id'] = ','.join(chunk)
            response = requests.get(url, params=payload).json()['items']
            result.extend(response)        
        return result

    def get_all_videos(self, channel_ids):
        """
        Retrieve all video infos from a list of channels.

        Parameters
        ----------
        channel_ids: list
            A list of Youtube channel IDs.

        Returns
        -------
        list
            A list of video objects. 
        """
        print("Extraindo vídeos de {} canais...".format(len(channel_ids)))
        result = []
        for channel_id in tqdm_notebook(channel_ids):   
            video_ids = self.get_video_ids(channel_id)
            result.extend(self.get_videos(video_ids))
        print("Pronto. {} vídeos foram extraídos.".format(len(result)))
        return result

    def get_comment_thread(self, video_id):
        """
        (UNDER CONSTRUCTION)

        Return comments thread of a Youtube video. Replies are also included, but restricted to the 5 first messages.
        """
        url = self.path + 'commentThreads'
        payload = {'part': 'id,replies,snippet',
                   'videoId': video_id,
                   'order': 'relevance',
                   'key': self.api_key}
        response = requests.get(url, params=payload).json()
        return response