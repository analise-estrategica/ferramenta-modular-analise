## Ferramenta modular de extração e armazenamento de dados

Os documentos presentes neste repositório tem função de instruir e suportar as atividades de exploração e extração em fontes de dados e armazenamento no banco de dados da área de Inovação Estratégica, sobretudo utilizando Python, MongoDB+PyMongo e o ambiente de desenvolvimento Jupyter Notebook. 

### Considerações aos procedimentos

#### Armazenamento
O processo de inserção no sistema de base de dados MongoDB não requer classes de suporte devido a simplicidade de seus comandos.
Para entender como os dados são inseridos na base de dados interna, consulte o tutorial de armazenamento de dados.

#### Consulta
Como os dados já inseridos na base, inicia-se o processo de consulta. Assim como o processo de armazenamento, sua manipulação é realizada apenas pelos comandos do PyMongo.
Para entender como extrair informações do banco de dados interno, examine o tutorial de consulta de dados.

#### Extração
O processo de extração é condicionado a fonte de dados de interesse. De forma geral, o fluxograma abaixo descreve a abordagem para um novo sistema de dados.
![Fluxograma_extração](https://trello-attachments.s3.amazonaws.com/5a5cf5523a75ea31bcde8a80/5a5f52a80f4574d4421c7e24/2fc5f4de29b4c22cf7d00b428872dc90/Estrutura_de_extra%C3%A7%C3%A3o.png)

O processo de documentação para a extração de dados recorrentes deve conter uma rotina de extração replicável e um registro dos parâmetros de busca caso a fonte seja uma API não documentada.
A estrutura das APIs não documentadas é exposta abaixo.