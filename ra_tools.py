
# coding: utf-8

# In[100]:


import requests
import sys
sys.path.insert(0, '../')
from beautifier import RenderJSON

class RA(object):
    def __init__(self):
        self._SEARCH = {'URL':"/raichu-io-site-search-0.0.1-SNAPSHOT/companies/search/", 'HOST':'iosearch.reclameaqui.com.br'}
        self._SHORTNAME = {'URL':'/raichu-io-site-0.0.1-SNAPSHOT/company/shortname/', 'HOST':'iosite.reclameaqui.com.br'}
        self._COMPLAINT = {'URL':'/raichu-io-site-0.0.1-SNAPSHOT/complain/', 'HOST':'iosite.reclameaqui.com.br'}
        self._HEADER = {
            'Accept':'application/json, text/plain, */*',
            'Accept-Encoding':'gzip, deflate, br',
            'Accept-Language':'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7',
            'Cache-Control':'no-cache',
            'Connection':'keep-alive',
            'Host':'',
            'Origin':'https://www.reclameaqui.com.br',
            'Pragma':'no-cache',
            'Referer': '',
            'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'
        }
        self._REQ = 'https://'
        self._REQ_ID = '?company='
    
    def get_complaints(self, companies, limit = None, preview = True):
        
        """
        Retrieve information about the .

        Parameters
        ----------
        channel_ids: str or list
            A list or a single company name.
        
        limit (optional): int
            A specified quantity of complaints. Companies which have less than the provided limit will return its maximum amount.
        
        preview: boolean
            A prefixed parameter (True) used to avoid unentional uploads to the database.
            To insert the results in the database, set preview to 'False'.
            
        Returns
        -------
            1: A list of search results of the listed companies
            2: A list of companies' evaluations in RA plataform
            3: A list of companies' complains in RA plataform
        """
        
        self._COMPANY_NAME = [companies] if type(companies) is str else companies
        self._NUM_COMPLAINTS = limit
        self._NUM_COMPLAINTS_PAG = self._NUM_COMPLAINTS if self._NUM_COMPLAINTS < 50 and self._NUM_COMPLAINTS is not None else 50
        self._ID = {'URL':'/raichu-io-site-search-0.0.1-SNAPSHOT/query/companyComplains/'+str(self._NUM_COMPLAINTS_PAG)+'/', 'HOST': 'iosearch.reclameaqui.com.br'}
        CONTENT_SEARCH, CONTENT_SN, CONTENT_ID = [], [], []
    
        for company in self._COMPANY_NAME:
            #print(companies)
            #Realizar a busca pela empresa e extrair o 'shortname' e 'id' do resultado mais relevante
            self._HEADER['Host'] = self._SEARCH['HOST']
            self._HEADER['Referer'] = self._REQ+self._SEARCH['HOST']+self._SEARCH['URL']+company
            RESPONSE = requests.get(self._HEADER['Referer'], headers=self._HEADER)
            CONTENT_SEARCH.append(RESPONSE.json())

            #Extrair o 'shortname' e o 'id'
            COMAPNY_ID = RESPONSE.json()['companies'][0]['id']
            COMPANY_SHORTNAME = RESPONSE.json()['companies'][0]['shortname']

            #Realizar a busca pelo 'shortname'
            self._HEADER['Host'] = self._SHORTNAME['HOST']
            self._HEADER['Referer'] = self._REQ+self._SHORTNAME['HOST']+self._SHORTNAME['URL']+COMPANY_SHORTNAME
            RESPONSE = requests.get(self._HEADER['Referer'], headers=self._HEADER)
            CONTENT_SN.append(RESPONSE.json())

            #Realizar a busca pelo 'id'
            self._HEADER['Host'] = self._ID['HOST']
            self._HEADER['Referer'] = self._REQ+self._ID['HOST']+self._ID['URL']+'0'+self._REQ_ID+COMAPNY_ID
            RESPONSE = requests.get(self._HEADER['Referer'], headers=self._HEADER)
            self._COMPLAINTS = requests.get(self._HEADER['Referer'], headers=self._HEADER).json()
            result_difference = self._NUM_COMPLAINTS-self._NUM_COMPLAINTS_PAG
            result_quantity = self._NUM_COMPLAINTS_PAG
            data = [1]
            pag = 1
            while len(data) > 0 and result_difference > 0:
                self._HEADER['Referer'] = self._REQ + self._ID['HOST'] + self._ID['URL'] + str(pag) + self._REQ_ID + COMAPNY_ID
                data = requests.get(self._HEADER['Referer'], headers=self._HEADER).json()['complainResult']['complains']['data']
                self._COMPLAINTS['complainResult']['complains']['data'].extend(data)
                result_quantity += self._NUM_COMPLAINTS_PAG
                result_difference = self._NUM_COMPLAINTS - result_quantity
                pag+=1
            if result_difference < 0:
                temp_url  = '/raichu-io-site-search-0.0.1-SNAPSHOT/query/companyComplains/'+str(abs(result_difference))+'/', 
                self._HEADER['Referer'] = self._REQ + self._ID['HOST'] + temp_url + str(pag) + self._REQ_ID + COMAPNY_ID
                data = requests.get(self._HEADER['Referer'], headers=self._HEADER).json()['complainResult']['complains']['data']
                self._COMPLAINTS['complainResult']['complains']['data'].extend(data)          
            CONTENT_ID.append(self._COMPLAINTS)
        if preview == True:
            return CONTENT_SEARCH, CONTENT_SN, CONTENT_ID
        else:
            #Discutir a inserção automática dos dados com JCL
            print('Under construction')