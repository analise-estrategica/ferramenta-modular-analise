from bs4 import BeautifulSoup
from time import gmtime, strftime
from fake_useragent import UserAgent
import csv
import datetime
import json
import re
import sys
import random
import requests
import pandas as pd


class TW():
    """
    Tool for scrappe data from Twitter.

    Parameters
    ----------
    self.api_key : str
        YouTube API key to consume data.
    """

    def __init__(self):
        self.__kwargs = {}
    def getTweets(self, **kwargs):
        '''
        Parameters:
            u : str or list
                User's Tweets you want to scrape
            s : str or list
                Search for Tweets containing this word or phrase
            g : str
                Search for geocoded tweets
            year : int
                Filter Tweets before specified year
            since : str
                Filter Tweets sent since date (Example: 2017-12-27)
            fruit : boolean
                Display 'low-hanging-fruit' Tweets.
            verified : boolean
                Display Tweets only from verified users (Use with -s)
            userid : int
                Twitter user id
            limit : int
                Number of Tweets to pull
            count : boolean
                Display number Tweets scraped at the end of session
        '''
        if 's' not in kwargs and 'u' not in kwargs:
            self.__Error('Lack of parameters', 'Provide at least one user (u) or one search term (s)')
        self.__kwargs.update(kwargs)
        self.__main()
        columns = ['id', 'date', 'time', 'timezone', 'user', 'tweet','hashtags', 'replies', 'retweets', 'likes']
        try:
            df = pd.read_csv(self.__kwargs['o']+'.csv', delimiter='|', encoding='utf-8', names=columns)
        except:
            df = pd.DataFrame(columns=columns)
        if 'delete' in kwargs.keys() and kwargs['delete'] == True:
            os.remove(self.__kwargs['o'])
        return df
    
    def getInfo(self, **kwargs):
        '''
        Parameters:
            u : str or list
                User's Tweets you want to scrape
        '''
        if 'u' not in kwargs:
            self.__Error('Lack of parameters', 'Provide at least one user (u) or one search term (s)')
        self.__kwargs.update(kwargs)
        ua = UserAgent()
        HEADERS_LIST = [ua.chrome, ua.google, ua['google chrome'], ua.firefox, ua.ff]
        headers = {'User-Agent': random.choice(HEADERS_LIST)}
        if type(self.__kwargs['u']) == str:
            users = [self.__kwargs['u']]
        else:
            users = self.__kwargs['u']
        df = pd.DataFrame({'user':[], 'tweets': [], 'following': [], 'followers': [], 'likes': []})
        for u in users:
            url = 'https://twitter.com/'+str(u.replace('@',''))
            tag = 'ProfileNav-list'
            content = requests.get(url, headers=headers).content
            parsed = BeautifulSoup(content, "lxml")
            results = {'tweets': 'tweets', 'following': 'following', 'followers': 'followers', 'likes': 'favorites'}
            for key, value in results.items():
                try: 
                    results[key] = int(parsed.select_one("a[data-nav*="+value+"]").select_one('span[class*="ProfileNav-value"]')['data-count'])
                except:
                    results[key] = 0
            results['user'] = u
            df.loc[len(df)] = results  
        return df
    
    
    
    def __Error(self, error, message):
        print("[-] {}: {}".format(error, message))
        sys.exit(0)
    
    def __getUrl(self):
        if list(self.__kwargs.keys()) == []:
            self.__Error("No inputs", "Please provide at least one input")
        if self.init == -1:
            url = "https://twitter.com/search?f=tweets&vertical=default&lang=en&q="
        else:
            url = "https://twitter.com/i/search/timeline?f=tweets&vertical=default"
            url+= "&lang=en&include_available_features=1&include_entities=1&reset_"
            url+= "error_state=false&src=typd&max_position={}&q=".format(self.init)
        additional_queries = {
            'u': "from%3A{}",
            'g': "geocode%3A{}",
            's': "%20{}",
            'until':"%20until%3A{}-1-1",
            'since': "%20since%3A{}",
            'fruit': "%20myspace.com%20OR%20last.fm%20OR%20mail%20OR%20email%20OR%20gmail%20OR%20e-mail%20OR%20phone%20OR%20call%20me%20OR%20text%20me%20OR%20keybase",
            'verified': "%20filter%3Averified"
        }
        for i in range(len(list(self.__kwargs.keys()))):
            try:
                key = list(self.__kwargs.keys())[i]
                value = self.__kwargs[key]
                if value != '':
                    url+=additional_queries[key].format(value).replace(" ", "%20").replace("#", "%23")
            except:
                pass
        return url
    
    def __getTweets(self):
        self.__getFeed()
        for tweet in self.tweets:
            if 'limit' in self.__kwargs and self.__count >= self.__kwargs['limit']:
                break
            tweetid = tweet["data-item-id"]
            datestamp = tweet.find("a", "tweet-timestamp")["title"].rpartition(" - ")[-1]
            d = datetime.datetime.strptime(datestamp, "%d %b %Y")
            date = d.strftime("%Y-%m-%d")
            timestamp = str(datetime.timedelta(seconds=int(tweet.find("span", "_timestamp")["data-time"]))).rpartition(", ")[-1]
            t = datetime.datetime.strptime(timestamp, "%H:%M:%S")
            time = t.strftime("%H:%M:%S")
            username = tweet.find("span", "username").text.replace("@", "")
            timezone = strftime("%Z", gmtime())
            text = tweet.find("p", "tweet-text").text.replace("\n", " ").replace("http"," http").replace("pic.twitter"," pic.twitter")
            hashtags = ",".join(re.findall(r'(?i)\#\w+', text, flags=re.UNICODE))
            replies = tweet.find("span", "ProfileTweet-action--reply u-hiddenVisually").find("span")["data-tweet-stat-count"]
            retweets = tweet.find("span", "ProfileTweet-action--retweet u-hiddenVisually").find("span")["data-tweet-stat-count"]
            likes = tweet.find("span", "ProfileTweet-action--favorite u-hiddenVisually").find("span")["data-tweet-stat-count"]
            try:
                mentions = tweet.find("div", "js-original-tweet")["data-mentions"].split(" ")
                for i in range(len(mentions)):
                    mention = "@{}".format(mentions[i])
                    if mention not in text:
                        text = "{} {}".format(mention, text)
            except:
                pass

            dat = [tweetid, date, time, timezone, username, text, hashtags, replies, retweets, likes]
            with open(self.__kwargs['o']+'.csv', "a+", newline='', encoding='utf-8') as csv_file:
                writer = csv.writer(csv_file, delimiter="|")
                writer.writerow(dat)
            self.__count += 1


    def __fetch(self, session, url):
            ua = UserAgent()
            HEADERS_LIST = [ua.chrome, ua.google, ua['google chrome'], ua.firefox, ua.ff]
            headers = {'User-Agent': random.choice(HEADERS_LIST)}
            response = session.get(url, headers=headers)
            return response.text

    def __getFeed(self):
        with requests.Session() as session:
            r = self.__fetch(session, self.__getUrl())
        self.tweets = []
        try:
            if self.init == -1:
                html = r
            else:
                json_response = json.loads(r)
                html = json_response["items_html"]
            soup = BeautifulSoup(html, "html.parser")
            self.tweets = soup.find_all("li", "js-stream-item")
            if self.init == -1:
                self.init = "TWEET-{}-{}".format(self.tweets[-1]["data-item-id"], self.tweets[0]["data-item-id"])
            else:
                split = json_response["min_position"].split("-")
                split[1] = self.tweets[-1]["data-item-id"]
                self.init = "-".join(split)
        except:
            pass


    def __getPic(self, url):
        with requests.Session() as session:
            r = self.fetch(session, url)
        soup = BeautifulSoup(r, "html.parser")
        picture = soup.find("div", "AdaptiveMedia-photoContainer js-adaptive-photo ")
        if picture is not None:
            return picture["data-image-url"].replace(" ", "")
    
    def __main(self):
        if 'o' not in self.__kwargs:
            hash = random.getrandbits(128)
            temp_file = "%032x" % hash
            self.__kwargs['o'] = temp_file
        
        if 'u' in self.__kwargs:
            if type(self.__kwargs['u']) is str:
                self.__kwargs['users'] = [self.__kwargs['u']]
            else:
                self.__kwargs['users'] = self.__kwargs['u']
        else:
            self.__kwargs['users'] = ['']
        
        if 's' in self.__kwargs and type(self.__kwargs['s']) is str:
            self.__kwargs['char'] = [self.__kwargs['s']]
        elif 's' in self.__kwargs:
            self.__kwargs['char'] = self.__kwargs['s']
        else:
            self.__kwargs['char'] = ['']
        
        for s in self.__kwargs['char']:
            for u in self.__kwargs['users']:
                self.__count = 0
                self.__kwargs['u'] = u
                self.__kwargs['s'] = s
                self.tweets = [-1]
                self.init = -1
                while True:
                    if len(self.tweets) > 0:
                        self.__getTweets()
                    else:
                        break
                if 'count' in self.__kwargs:
                    print("Finished: Successfully collected {} Tweets.".format(self.__count))
        
    def getReach(self, **kwargs):
        '''
        Parameters:
            u : str or list
                User's Tweets you want to scrape
            s : str or list
                Search for Tweets containing this word or phrase
            limit : str or list
                Number of Tweets to pull
            df (optional) : pandas DataFrame
                Dataframe of tweets previously scrapped with 'getTweets' method
        '''
        if 'df' not in kwargs:
            if 's' not in kwargs and 'u' not in kwargs:
                self.__Error('Lack of parameters', 'Provide users (u) or search terms (s)')
            else:
                s = '' if 's' not in kwargs else kwargs['s']
                u = '' if 'u' not in kwargs else kwargs['u']
                limit = 0 if 'limit' not in kwargs else kwargs['limit']
                df = self.getTweets(s=s, u=u, limit=limit)
        df.user_followers = 0
        df_info = tw.getInfo(u=list(df['user'].unique()))
        for i in range(len(df_info)):
            df.loc[(df.user == df_info.loc[i,'user']),'user_followers'] = int(df_info.loc[i,'followers'])
        return df